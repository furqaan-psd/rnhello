import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const App = () => {
  return (
    <View style={styles.body}>
      <Text style={styles.txtView}>Hello world!!!</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center'
  },
  txtView: {
    marginHorizontal: 100,
    alignContent: 'center',
    fontSize: 30,
  },
});

export default App;